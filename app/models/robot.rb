class Robot
  def initialize(params)
    @setup = params
    @position_x = params[:start_x].to_i
    @position_y = params[:start_y].to_i 
  end

  def move
    return "configuração inválida" unless validate_location

    x = @position_x
    y = @position_y

    moves = @setup[:moves].split(" ")
    moves.each do |move|
      steps = move[0, move.size-1].to_i
      direction = move[move.size-1, move.size]

      y = @position_y + steps if direction == "N"
      y = @position_y - steps if direction == "S"
      x = @position_x + steps if direction == "L"
      x = @position_x - steps if direction == "O"

      break unless validate_position(x, y)
        
      @position_x = x
      @position_y = y
    end

    "Inicio (#{@setup[:start_x]}, #{@setup[:start_y]}) / Movimento [#{@setup[:moves]}] / Posição Final: (#{@position_x},#{@position_y})"
  end

  def validate_position(x, y)
    valid = x <= @setup[:size_x].to_i && x >= 0
    valid &= y <= @setup[:size_y].to_i && y >= 0
  end

  def validate_location
    valid = @setup[:size_x].to_i > 0 && @setup[:size_x].to_i < 1_000_000_000
    valid &= @setup[:size_y].to_i > 0 && @setup[:size_y].to_i < 1_000_000_000
    valid &= validate_position(@setup[:start_x].to_i, @setup[:start_y].to_i)
  end
end