class RobotsController < ApplicationController
  def show
  end

  def new
    @message = params[:message] if params[:message]
    @robot = nil
  end

  def create
    @robot = Robot.new(params)

    redirect_to new_robot_path(message: "#{@robot.move}")
  end
end
